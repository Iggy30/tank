// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GBTankGameMode.generated.h"

UCLASS(minimalapi)
class AGBTankGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGBTankGameMode();
};



