// Copyright Epic Games, Inc. All Rights Reserved.

#include "GBTankGameMode.h"
#include "GBTank/Characters/GBTankCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGBTankGameMode::AGBTankGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
